// --------------------------------------------------------------------------------------------------------------------

'use strict'

// npm
const test = require('tape')
const request = require('request')

// --------------------------------------------------------------------------------------------------------------------

const PORT = process.env.PORT || 33515

// const baseUrl = 'http://postb.in'
const baseUrl = `http://localhost:${PORT}`

// `id` is like '1558003097958-3371436139568'
const idRegExp = new RegExp(/^\d{13}-\d{13}$/)
const binIdLength = 27

let binId
let reqId1

test('creating a bin', function (t) {
  t.plan(5)

  // firstly, make a bin
  request.post(baseUrl + '/api/bin', function (err, res, body) {
    t.equal(err, null, 'There was no error when creating the bin')
    t.equal(res.statusCode, 201, 'We get a CREATED status code back')
    t.equal(res.headers['content-type'], 'application/json; charset=utf-8', 'Returned body is JSON')

    // parse the body
    body = JSON.parse(body)
    t.equal(body.binId.length, binIdLength, `The binId is a string of ${binIdLength} chars`)
    t.ok(body.binId.match(idRegExp), 'The binId is just a yid()')

    // save this binId for subsequent requests
    binId = body.binId

    t.end()
  })
})

test('get info for this bin', function (t) {
  t.plan(4)

  // firstly, make a bin
  request.get(baseUrl + '/api/bin/' + binId, function (err, res, body) {
    t.equal(err, null, 'There was no error when creating the bin')
    t.equal(res.statusCode, 200, 'We get an OK status code back')
    t.equal(res.headers['content-type'], 'application/json; charset=utf-8', 'Returned body is JSON')

    // parse the body
    body = JSON.parse(body)
    t.equal(body.binId, binId, 'The binId is correct')

    t.end()
  })
})

test('posting to a bin', function (t) {
  t.plan(4)

  // firstly, make a bin
  request(baseUrl + '/' + binId, function (err, res, body) {
    t.equal(err, null, 'There was no error when creating the bin')
    t.equal(res.statusCode, 200, 'We get an OK status code back')
    t.equal(res.headers['content-type'], 'text/plain; charset=utf-8', 'Returned body is plaintext')

    // parse the body
    t.ok(body.match(idRegExp), 'The returned body is correct')

    // save this reqId for subsequent requests
    reqId1 = body

    t.end()
  })
})

test('get this request back from the API', function (t) {
  t.plan(18)

  function testReq1 (err, res, json) {
    t.equal(err, null, 'There was no error with this request')
    t.equal(res.statusCode, 200, 'We get a OK status code back')
    t.equal(res.headers['content-type'], 'application/json; charset=utf-8', 'Returned body is JSON')

    // parse the returned body (since it is JSON)
    const body = JSON.parse(json)

    t.equal(body.method, 'GET', 'We sent a GET and got it back')
    t.deepEqual(body.query, {}, 'No query sent')
    t.deepEqual(body.body, {}, 'No body sent')
    t.deepEqual(body.path, `/${binId}`, 'Path is correct')
    t.equal(body.binId, binId, 'binId is correct')
    t.equal(body.reqId, reqId1, 'reqId is correct')
  }

  request(baseUrl + '/api/bin/' + binId + '/req/' + reqId1, function (errGet, resGet, bodyGet) {
    testReq1(errGet, resGet, bodyGet)

    // let's do it again, but this time do a shift
    request(baseUrl + '/api/bin/' + binId + '/req/shift', function (errShift, resShift, bodyShift) {
      testReq1(errShift, resShift, bodyShift)
      t.end()
    })
  })
})

test('If we shift this bin again, there is nothing there', function (t) {
  t.plan(4)

  // let's do it again, but this time do a shift
  request(baseUrl + '/api/bin/' + binId + '/req/shift', function (errShift, resShift, bodyShift) {
    t.equal(errShift, null, 'There was no error with this request')
    t.equal(resShift.statusCode, 404, 'We get a NOT FOUND status code back')
    t.equal(resShift.headers['content-type'], 'application/json; charset=utf-8', 'Returned body is JSON')

    const body = JSON.parse(bodyShift)
    t.deepEqual(body, { msg: 'No requests in this bin' }, 'Returned body and message is correct')

    t.end()
  })
})

test('if we try to get this reqId again, there is nothing there', function (t) {
  t.plan(4)

  // let's do it again, but this time do a shift
  request(baseUrl + '/api/bin/' + binId + '/req/' + reqId1, function (errShift, resShift, bodyShift) {
    t.equal(errShift, null, 'There was no error with this request')
    t.equal(resShift.statusCode, 404, 'We get a NOT FOUND status code back')
    t.equal(resShift.headers['content-type'], 'application/json; charset=utf-8', 'Returned body is JSON')

    const body = JSON.parse(bodyShift)
    t.deepEqual(body, { msg: 'No such request' }, 'This request does not exist')

    t.end()
  })
})

test('delete this bin', function (t) {
  t.plan(4)

  request.del(baseUrl + '/api/bin/' + binId, function (err, res, body) {
    t.equal(err, null, 'There was no error with this request')
    t.equal(res.statusCode, 200, 'We get a OK status code back')
    t.equal(res.headers['content-type'], 'application/json; charset=utf-8', 'Returned body is JSON')

    body = JSON.parse(body)
    t.deepEqual(body, { msg: 'Bin deleted' }, 'Returned body and message is correct')

    t.end()
  })
})

test('testing the bin is no longer there', function (t) {
  t.plan(4)

  // try and get this bin back
  request.get(baseUrl + '/api/bin/' + binId, function (err, res, body) {
    t.equal(err, null, 'There was no error when creating the bin')
    t.equal(res.statusCode, 404, 'We get a NOT FOUND status code back')
    t.equal(res.headers['content-type'], 'application/json; charset=utf-8', 'Returned body is JSON')

    // parse the body
    body = JSON.parse(body)
    t.deepEqual(body, { msg: 'No such bin' }, 'The body is correct')

    t.end()
  })
})

// --------------------------------------------------------------------------------------------------------------------

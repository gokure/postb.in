// --------------------------------------------------------------------------------------------------------------------

'use strict'

// core
const path = require('path')

// npm
const express = require('express')
const favicon = require('serve-favicon')
const morgan = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const responseTime = require('response-time')
const errorHandler = require('errorhandler')
const moment = require('moment')

// local
const pkg = require('../package.json')
const env = require('./env.js')
const banned = require('./banned.js')
const stats = require('./stats.js')

// --------------------------------------------------------------------------------------------------------------------
// the application

// setup
const app = express()
app.set('views', path.join(__dirname, '..', 'views'))
app.set('view engine', 'pug')
app.enable('trust proxy')
app.enable('strict routing')
app.set('case sensitive routing', true)
app.disable('x-powered-by')

// set some locals that don't change when the app is running
app.locals.pkg = pkg
app.locals.env = {
  isProd: env.isProd,
  baseUrl: env.baseUrl,
  min: env.min,
  googleAnalytics: env.googleAnalytics
}
app.locals.moment = moment

app.use((req, res, next) => {
  if (req.ip in banned) {
    stats.banned.inc()
    res.status(403).send('403 - Forbidden. This IP address is banned for excessive misuse. Please contact https://twitter.com/andychilton.')
    return
  }

  console.log(`ip=${req.ip}`)
  next()
})

// middleware
app.use(responseTime({ digits: 5 }))
app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')))
app.use(morgan(env.isProd ? 'combined' : 'dev'))
app.use(express.static(path.join(__dirname, '..', 'public')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

// routes
require('./routes/home.js')(app)
require('./routes/api.js')(app)
require('./routes/inspect.js')(app)
require('./routes/bin.js')(app)

// error middleware
if (env.isProd) {
  // production middleware
  app.use((err, req, res, next) => {
    if (err) {
      console.warn('err:', err)
      res.status(500).send('500 - Internal Server Error\n')
      return
    }
    next(err)
  })
} else {
  // development only
  app.use(errorHandler())
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = app

// --------------------------------------------------------------------------------------------------------------------

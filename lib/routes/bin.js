// --------------------------------------------------------------------------------------------------------------------

'use strict'

// local
var middleware = require('../middleware.js')
var db = require('../db.js')
var stats = require('../stats.js')

// --------------------------------------------------------------------------------------------------------------------

// From : https://devcenter.heroku.com/articles/http-routing#heroku-headers
var headersToRemove = [
  'x-forwarded-for',
  'x-forwarded-proto',
  'x-forwarded-port',
  'x-request-start',
  'x-request-id',
  'via'
]

function createReq (req, res, next) {
  stats.createReq.inc()

  var info = {
    method: req.method,
    path: req.path,
    headers: req.headers,
    query: req.query,
    body: req.body,
    ip: req.ip
  }

  headersToRemove.forEach(function (hdr) {
    delete info.headers[hdr]
  })

  db.insReq(res.locals.bin.binId, info, function (err, result) {
    if (err) return next(err)
    res.setHeader('Content-Type', 'text/plain')
    res.send(result.reqId)
  })
}

module.exports = function (app) {
  app.all(
    '/:binId',
    middleware.getBin,
    createReq
  )

  app.all(
    '/:binId/*',
    middleware.getBin,
    createReq
  )
}

// --------------------------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------

// npm
const redis = require('redis')
const rustle = require('rustle')

// local
const pkg = require('../package.json')

// ----------------------------------------------------------------------------

// redis
const client = redis.createClient()

const stats = {}

const events = [
  'home',
  'banned',
  'createBin',
  'createReq',
  'inspect',
  'apiGetBin',
  'apiCreateBin',
  'apiDelBin',
  'apiShiftReq',
  'apiGetReq',
  'apiGetBin404',
  'apiGetReq404'
]
events.forEach(function (name) {
  stats[name] = rustle({
    client: client,
    // Keys: "<domain>:<category>:<name>"
    domain: pkg.name,
    category: 'hits',
    name: name,
    period: 24 * 60 * 60, // one day
    aggregation: 'sum'
  })
})

stats.events = events

// ----------------------------------------------------------------------------

module.exports = stats

// ----------------------------------------------------------------------------
